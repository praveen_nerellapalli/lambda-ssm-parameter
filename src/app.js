/*! Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *  SPDX-License-Identifier: MIT-0
 */

// 1. Receive event with method and body.
// 2. GET or PUT an SSM Parameter Store parameter.
// 3. Return a response with parameter result.

const AWS = require('aws-sdk')
const cfnResponse = require('cfn-response')
const ssm = new AWS.SSM({ region: 'us-east-2' })

exports.handler = (event, context) => {
  try {
    // Log event and context object to CloudWatch Logs
    console.log("Event:\n", JSON.stringify(event, null, 2))
    console.log("Context:\n", JSON.stringify(context, null, 2))
    if (event.RequestType == "Delete") {
      cfnResponse.send(event, context, cfnResponse.SUCCESS)
    }
    // Get method and body from event
    const parameterName = event.ResourceProperties.ParameterName
    console.log("SSMParameterName: ", parameterName);
    const parameterValue = event.ResourceProperties.ParameterValue
    console.log("SSMParameterValue: ", parameterValue)

    const ssmPutParams = {
      Name: parameterName,
      Value: parameterValue,
      Overwrite: true,
      Type: "String",
    }

    ssm.putParameter(ssmPutParams, function(err, data) {
      if (err) {
        console.log(err, err.stack)
      }
      else {
        console.log("Result:\n", JSON.stringify(data));
        const response = {
          statusCode: 200,
          body: JSON.stringify(data)
        }
        cfnResponse.send(event, context, cfnResponse.SUCCESS, response)
      }
    })

  }
  catch (error) {
    cfnResponse.send(event, context, cfnResponse.FAILED, error)
    console.error(error)
    throw new Error(error)
  }
}
